# nuom Programming Challenge

... INSERT TASK HERE ...

Optional: In order to demonstrate reactive programming in this example you could
add search functionality to the list of contacts. You could showcase this by
adding a search bar, underneath the navigation bar on the main view. Then use
streams to process the search terms and present the relevant results.

## Setup

1. Clone this repo
2. Update the git submodule: `$ git submodule update --init --recursive`
3. Open `Programming Task/Programming Task.xcodeproj` in Xcode and have fun!

## Documentation

The API is hosted here: https://nuom-staff.herokuapp.com

You can find the relevant documentation here: https://nuomstaff.docs.apiary.io

## What we will be looking for

- Good practices and recommended iOS guidelines being followed
- Object-orientated programming principles
- Standard naming conventions
- Easy to read and sensibly organised code structure
- Appropriate use of comments

A rough wireframe is attached of how this solution could look, however you
should follow iOS design guidelines when developing the application. Clean
code, good architecture, good UX, performance improvements are the main points.
Please include notes with your project using the `YOUR_NOTES.md` file.

This is your time to show off your skills! We look forward to reviewing your
code.
